﻿using System.ComponentModel.DataAnnotations;
namespace Jual_Beli.Models
{
    public class AbsenCoy
    {
        [Required]
        public string nama { get; set; }
        [Required]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Masukkan Email dengan benar")]
        public string email { get; set; }
        [Required]
        public string nomorhp { get; set; }
        [Required]
        [RegularExpression("^\\s*(3[01]|[12][0-9]|0?[1-9])\\/(1[012]|0?[1-9])\\/((?:19|20)\\d{2})\\s*$", ErrorMessage = "Format : dd/mm/yyyy")]
        public string waktu { get; set; }
        [Required]
        public bool konfirmasi { get; set; }
    }
}
