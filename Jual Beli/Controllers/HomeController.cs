﻿using Jual_Beli.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Jual_Beli.Controllers
{
	public class HomeController : Controller
	{
		/*public string Index()
		{
			return "Selamat Datang di Latihan MVC Patternku";
		}*/

		public ActionResult Index()
		{ 
			int jam = DateTime.Now.Hour;
			string pemagang = "Rayhan Hakeem";
			
			if(9 <= jam && jam <= 12)
			{
				ViewBag.Pesan = $"Selamat Pagi {pemagang}, semangat bekerja!";
			}else if(12 < jam && jam >= 14)
			{
				ViewBag.Pesan = $"Selamat Siang {pemagang}, tetap semangat bekerja!";
			} else if(14 < jam && jam >= 17)
			{
				ViewBag.Pesan = $"Selamat Sore {pemagang}, masih semangat kan? dikit lagi selesaii ayoo!";
			}
			else
			{
				ViewBag.Pesan = $"Sekarang Waktunya istirahat, selamat istirahat ya, {pemagang}";
			}

			return View();
		}

		public ActionResult FormAbsen(Models.AbsenCoy absenCoy)
		{
			if(ModelState.IsValid)
			{
				return View("Makasih", absenCoy);
			}
			return View();
		}
	}
}